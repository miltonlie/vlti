#include <sys/stat.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/select.h>

#define BUFFLEN   1300
#define SOCK_PORT 1201
#define TUN_NAME  "tun1"

/*
 * Utilities
 */
const char HEX[] =
{
 '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
 'A', 'B', 'C', 'D', 'E', 'F',
};

void hex(char* source, char* dest, int len)
{
  for (int i = 0; i < len; ++i) {
    unsigned char data = source[i];
    dest[2*i]     = HEX[data >> 4];
    dest[2*i + 1] = HEX[data & 0xF];
  }
  dest[2*len] = '\0';
}

void dump_ip_header_info(const char* fd_name, int len, char* buffer)
{
  if (len < 20) {
    fprintf(stderr, "IPv4 packet too short\n");
    return;
  }

  int total_length, ttl;
  char protocol_number[3];
  char *protocol;

  total_length = (buffer[2] << 8) + buffer[3];

  ttl = buffer[8];

  protocol_number[2] = '\0';
  protocol_number[1] = (buffer[9] & 0xF) + '0';
  protocol_number[0] = (buffer[9] >> 4)  + '0';

  protocol = (buffer[9] == 1) ? "ICMP" : ( (buffer[9] == 6) ? "TCP" : ( (buffer[9] == 16) ? "UDP" : protocol_number) );

  fprintf(stderr, "%s: total_lenght=%d, ttl=%d, protocol=%s, src_ip=%u.%u.%u.%u, dst_ip=%u.%u.%u.%u\n",
          fd_name, total_length, ttl, protocol,
          (unsigned char) buffer[12], (unsigned char) buffer[13], (unsigned char) buffer[14], (unsigned char) buffer[15],
          (unsigned char) buffer[16], (unsigned char) buffer[17], (unsigned char) buffer[18], (unsigned char) buffer[19]);
}

in_addr_t validate_ip_addr(const char* s_ip_addr)
{
  int count = 0;
  in_addr_t ip_addr;
  
  for (int i = 0; s_ip_addr[i] != '\0'; ++i) {
    if (s_ip_addr[i] == '.') {
      ++count;
    }
  }
  if (count != 3) {
    fprintf(stderr, "Invalid IP address : %s\n", s_ip_addr);
    exit(1);
  }

  if ( ( ip_addr = inet_addr(s_ip_addr) )== -1 ) {
    fprintf(stderr, "Invalid IP address : %s\n", s_ip_addr);
    exit(1);
  }

}

/*
 * Hardcoded to TUN_NAME for now.
 * Should parameterize to select TUN device
 */

int tun_alloc() {
  struct ifreq ifr;
  int fd, e;

  if ( (fd = open("/dev/net/tun", O_RDWR) ) < 0 ) {
    perror("Cannot open /dev/net/tun");
    return fd;
  }

  memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
  strncpy(ifr.ifr_name, TUN_NAME, IFNAMSIZ);

  if ( (e = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ) {
    perror("ioctl[TUNSETIFF]");
    close(fd);
    return e;
  }

  return fd;
}

void usage(void)
{
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "tunnel <local Unicast IP> <Publisher Multicast IP> <Subscriber Multicast IP>\n");
  exit(1);
}

int main(int argc, char** argv)
{
  char buffer[BUFFLEN];
  char buffer2[2*BUFFLEN + 1];

  int nread, nwrite;
  int ip_version;
  in_addr_t local_unicast_ip, pub_multicast_ip, sub_multicast_ip;
  int tun_fd, udp_fd;
  struct sockaddr_in sub_multicast_sockaddr, pub_multicast_sockaddr;
  int recv_len, send_len;
  struct ip_mreq group;

  if ( argc != 4 ) {
    usage();
  }

  const char* s_local_unicast_ip = argv[1];
  const char* s_pub_multicast_ip = argv[2];
  const char* s_sub_multicast_ip = argv[3];
  local_unicast_ip = validate_ip_addr(s_local_unicast_ip);
  pub_multicast_ip = validate_ip_addr(s_pub_multicast_ip);
  sub_multicast_ip = validate_ip_addr(s_sub_multicast_ip);

  // Create socket
  if ( (udp_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
    perror("Failed to create socket");
    exit(1);
  }

  // Details of local unicast IP
  memset( (char *) &sub_multicast_sockaddr, 0, sizeof(sub_multicast_sockaddr) );
  sub_multicast_sockaddr.sin_family      = AF_INET;          // IPv4
  sub_multicast_sockaddr.sin_addr.s_addr = sub_multicast_ip;
  sub_multicast_sockaddr.sin_port        = htons(SOCK_PORT);

  // Details of Pub Multicast IP
  memset( (char *) &pub_multicast_sockaddr, 0, sizeof(pub_multicast_sockaddr) );
  pub_multicast_sockaddr.sin_family      = AF_INET; // IPv4
  pub_multicast_sockaddr.sin_addr.s_addr = pub_multicast_ip;
  pub_multicast_sockaddr.sin_port        = htons(SOCK_PORT);

  // Bind socket to subscriber multicast IP for receiving IP packets from peer (publisher)
  if ( bind(udp_fd, (const struct sockaddr *)&sub_multicast_sockaddr, sizeof(sub_multicast_sockaddr)) < 0 ) {
    perror("Failed to bind to subscriber multicast IP");
    close(udp_fd);
    exit(1);
  }

  // Multicast setup

  // set the multicast interface to the local unicast ip
  struct in_addr multi_if_ip;
  multi_if_ip.s_addr = local_unicast_ip;

  if(setsockopt(udp_fd, IPPROTO_IP, IP_MULTICAST_IF, (char *)&(multi_if_ip), sizeof(multi_if_ip)) < 0)
  {
    perror("Setting local interface error");
    exit(1);
  }

  // make sure multicast loop is turned off
  char loopch = 0;
  if(setsockopt(udp_fd, IPPROTO_IP, IP_MULTICAST_LOOP, (char *)&loopch, sizeof(loopch)) < 0)
  {
    perror("Setting IP_MULTICAST_LOOP error");
    close(udp_fd);
    exit(1);
  }
  
  // join the subscriber multicast address group
  group.imr_multiaddr.s_addr = sub_multicast_ip;
  group.imr_interface.s_addr = local_unicast_ip;
  if(setsockopt(udp_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
  {
    perror("Adding multicast group error");
    close(udp_fd);
    exit(1);
  }

  // get fd for TUN_NAME
  if ( (tun_fd = tun_alloc()) < 0 ) {
    return tun_fd;
  }

  // Listen for data
  while (1) {
    // Prepare fd_set for reading tun device and udp socket
    fd_set readset;
    FD_ZERO(&readset);
    FD_SET(tun_fd, &readset);
    FD_SET(udp_fd, &readset);
    int max_fd = ( (tun_fd > udp_fd) ? tun_fd : udp_fd ) + 1;

    if ( select(max_fd, &readset, NULL, NULL, NULL) < 0 ) {
      perror("select failed");
      close(tun_fd);
      close(udp_fd);
      exit(1);
    }
    
    // Read an IP packet from tun device
    if ( FD_ISSET(tun_fd, &readset) ) {
      nread = read(tun_fd, buffer, BUFFLEN);
      if ( nread < 0 ) {
        perror("Reading tun device");
        close(tun_fd);
        close(udp_fd);
        exit(1);
      }

      // ignore IPv6 packets
      ip_version = buffer[0] >> 4;
      if ( ip_version == 4 ) {
        // Debug information
        dump_ip_header_info("tun_fd", nread, buffer);

        // forward over tunnel
        if ( (send_len = sendto(udp_fd, buffer, nread, 0,
                                (struct sockaddr *) &pub_multicast_sockaddr,
                                sizeof(pub_multicast_sockaddr)) < 0)        ) {
          perror("sendto failed");
          close(tun_fd);
          close(udp_fd);
          exit(1);
        }
      }
    }

    // Read an IP packet from udp socket
    if ( FD_ISSET(udp_fd, &readset) ) {
      if ( (recv_len = recvfrom(udp_fd, buffer, BUFFLEN, 0, NULL, NULL)) < 0 ) {
        perror("recvfrom failed");
        close(tun_fd);
        close(udp_fd);
        exit(1);
      }

      // Debug information
      dump_ip_header_info("udp_fd", recv_len, buffer);

      // Forward received IP packet to tun device,
      //  and let network stack forward the IP packet organically
      nwrite = write(tun_fd, buffer, recv_len);
      if ( nwrite < 0 ) {
        perror("Writing tun device");
        close(tun_fd);
        close(udp_fd);
        exit(1);
      }
    }
    
  }

  close(tun_fd);
  close(udp_fd);

  return 0;
}
