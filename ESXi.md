# install xrdp (remote desktop)
$ sudo apt install xrdp

# install ssh
sudo apt install openssh-server
sudo systemctl enable ssh
sudo systemctl start ssh

# install emacs
sudo apt install emacs-nox
# open & close emacs to create .emacs.d
scp dev@192.168.212.242.emacs.d/init.el .emacs.d/

# install build-essential
sudo apt install build-essential

# install cmake
sudo apt install cmake

# There are many ways to add environment variables.
Per https://help.ubuntu.com/community/EnvironmentVariables does not work when opening new shells without logout / login
Add following to ~/.bash_rc after .bash_aliases section

# local bash environment. This will load when opening new shells without logout / login

if [ -f ~/.bash_env ]; then
. ~/.bash_env
fi
