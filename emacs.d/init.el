(put 'narrow-to-region 'disabled nil)

;
; Custom keyboard macros
;
(global-set-key (kbd "C-\\") 'compare-windows)
(global-set-key (kbd "C-x c") 'shell)
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)

;; set default tab width to 2
(setq-default
  indent-tabs-mode nil
  tab-width 4
  tab-stoplist (quote (4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80 84 88 92 96 100 104 108 112 116 120)))

;; Disable backup files
(setq make-backup-files nil)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; For YAML mode - https://github.com/yoshiki/yaml-mode.git
;; (load "~/.emacs.d/yaml-mode.el")

