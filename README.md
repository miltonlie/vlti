Track all installs on ubuntu 20.04.3 LTS virtual machine.

# apt update
sudo apt update  
sudo apt upgrade

# install emacs without X
sudo apt install emacs-nox

# install git
sudo apt install git

# setup ssh keys for git@bitbucket.org:miltonlie/vlti.git
ssh-keygen

# first time setup for git
git config --global user.name "Milton Lie"  
git config --global user.email "mlie@bellflight.com"  
git config --global core.editor emacs  
git config --global http.sslVerify false

# setup openssh-server
sudo apt install openssh-server  
sudo systemctl enable ssh  
sudo systemctl start ssh  

# install build-essential
sudo apt install build-essential

# install cmake
sudo apt install cmake

# There many way to add environment variables.
Per https://help.ubuntu.com/community/EnvironmentVariables does not work when
opening new shells without logout / login  
Add following to ~/.bash_rc after .bash_aliases section  

\# local bash environment.  This will load when opening new shells without logout / login

if [ -f ~/.bash_env ]; then  
    . ~/.bash_env  
fi  

# Assume two instances of ubuntu 20.04.3: ubuntu-a and ubuntu-b
\# compile tunnel.c on both ubuntu-a and ubuntu-b  
gcc -o tunnel tunnel.c  
  
# ubuntu-a
\# eno1 10.12.3.8/10  
sudo ip addr add 172.16.11.1/32 dev lo  
sudo ip tuntap add dev tun1 mode tun user dev  
sudo ip link set dev tun1 mtu 1300  
sudo ip link set dev tun1 up  
sudo ip route add 172.16.21.1/32 dev tun1 src 172.16.11.1  
  
\# Usage:  
\# tunnel <local Unicast IP> <Publisher Multicast IP> <Subscriber Multicast IP>  
  
\# tunnel console  
./tunnel 10.12.3.8 239.227.10.16 239.227.10.17  
  
\# ping console  
ping 172.16.21.1  
  
# ubuntu-b
\# eno1 10.12.13.4/10  
\# eno2 192.168.114.28/25  
sudo ip addr add 172.16.21.1/32 dev lo  
sudo ip tuntap add dev tun1 mode tun user dev  
sudo ip link set dev tun1 mtu 1300  
sudo ip link set dev tun1 up  
sudo ip route add 172.16.11.1/32 dev tun1 src 172.16.21.1  
  
\# tunnel console  
./tunnel 10.12.13.4 239.227.10.17 239.227.10.16  
  
# Notes
\# show additional details like type and user for tuntap devices with -d switch  
ip -d addr  
ip -d link  



